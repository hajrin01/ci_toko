

<table width="40%" height="70" border="0" align="center">
  <center><h2 style="font-family:'Comic Sans MS', cursive">Input Karyawan</h2></center>
  
  <center><div style="color: red"><?= validation_errors(); ?></div></center>
<form action="<?=base_url()?>karyawan/inputkaryawan" method="POST">
<table width="40%" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <br />  
    <td>Nik</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" maxlength="10" value="<?= set_value('nik');?>" /></td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="50" value="<?= set_value('nama_lengkap');?>" /></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td> <input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" value="<?= set_value('tempat_lahir');?>" /></td>
    </td>
  </tr>
  <tr>
    <td height="35">Jenis Kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
       <option value="L">L</option>
       <option value="P">P</option>
      
    </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		?>
        <option value="<?=$tgl;?>"><?=$tgl;?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		?>
        <option value="<?=$bln+1;?>">
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
		?>
        <option value="<?=$thn;?>"><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?= set_value('telp');?>" /></td>
    </td>
  </tr>
 
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"></textarea></td>
  </tr>
 <tr>
    <td height="35">Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan">
    <?php foreach($data_jabatan as $data) {?>
       <option value="<?= $data->kode_jabatan;?>">
       <?= $data->nama_jabatan; ?></option>
       
      
      <?php }?>
      
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan">
      <input type="submit" name="batal" id="batal" value="reset">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a></td>
  </tr>
</table>
</table>
</form>

