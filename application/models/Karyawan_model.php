<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";

	public function rules()
{
		return[
		[
				'field' =>'nik',
				'label' =>'Nik',
				'rules' =>'required|max_length[10]',
				'errors' =>[
					'required' => 'Nik tidak boleh kosong.',
					'max_length' => 'Nik tidak boleh lebih dari 10 karakter.',
				]

		],
		[
				'field' =>'nama_lengkap',
				'label' =>'Nama Lengkap',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Nama Lengkap tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'tempat_lahir',
				'label' =>'Tempat Lahir',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Tempat Lahir tidak boleh kosong.',
					
				]
		],
		[
				'field' =>'jenis_kelamin',
				'label' =>'Jenis Kelamin',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Jenis Kelamin tidak boleh kosong.',
					
				]
		],
		[
				'field' =>'tgl','bln','thn',
				'label' =>'tgl lahir',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Tanggal Lahir tidak boleh kosong.',
					
				]

			],
		[
				'field' =>'telp',
				'label' =>'Telpon',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Telpon tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'alamat',
				'label' =>'Alamat',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Alamat tidak boleh kosong.',
					
				]
		],
		[
				'field' =>'kode_jabatan',
				'label' =>'Kode Jabatan',
				'rules' =>'required',
				'errors' =>[
					'required' => 'kode Jabatan tidak boleh kosong.',
					
				]
		        ]
		];
	}
	
	public function tampilDataKaryawan()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataKaryawan2()
	
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDataKaryawan3()
	
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function save()
	
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "_" .$bln . "_" . $tgl;
		
		$data['nik'] =$this->input->post('nik');
		$data['nama_lengkap'] =$this->input->post('nama_lengkap');
		$data['tempat_lahir'] =$this->input->post('tempat_lahir');
		$data['tgl_lahir'] =$tgl_gabung;
		$data['jenis_kelamin'] =$this->input->post('jenis_kelamin');
		$data['alamat'] =$this->input->post('alamat');
		$data['telp'] =$this->input->post('telp');
		$data['kode_jabatan'] =$this->input->post('kode_jabatan');
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);

	}
	public function detail($nik)
	
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function update($nik)
	
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "_" .$bln . "_" . $tgl;
		
		$data['nama_lengkap'] =$this->input->post('nama_lengkap');
		$data['tempat_lahir'] =$this->input->post('tempat_lahir');
		$data['tgl_lahir'] =$tgl_gabung;
		$data['jenis_kelamin'] =$this->input->post('jenis_kelamin');
		$data['alamat'] =$this->input->post('alamat');
		$data['telp'] =$this->input->post('telp');
		$data['kode_jabatan'] =$this->input->post('kode_jabatan');
		$data['flag'] = 1; 
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);

	}

	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);	
	}
	

}
